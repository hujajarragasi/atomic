<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDompet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dompet', function (Blueprint $table) {
            $table->integer('Id')->unique();
            $table->string('Nama');
            $table->string('Refrensi');
            $table->string('Deskripsi');
            $table->integer('Status_ID');           
            $table->timestamps();
        });
        Schema::table('dompet', function($table) {
            $table->foreign('Status_ID')->references('Id')->on('dompet_status');
        });
   
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dompet');
    }
}
