<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->integer('Id')->unique();
            $table->string('Kode');
            $table->string('Deskripsi');
            $table->datetime('Tanggal');
            $table->integer('nilai');
            $table->integer('Dompet_ID');
            $table->integer('Kategori_ID');
            $table->integer('Status_ID');

        });
        Schema::table('transaksi', function($table) {
            $table->foreign('Dompet_ID')->references('Id')->on('dompet');
            $table->foreign('Kategori_ID')->references('Id')->on('kategori');
            $table->foreign('Status_ID')->references('Id')->on('transaksi_status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
